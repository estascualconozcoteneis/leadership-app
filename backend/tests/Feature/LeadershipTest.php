<?php

namespace Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use Illuminate\Support\Facades\DB;

class leadershippApiTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();
        DB::unprepared(
            "
            CREATE TABLE  leaderships (
                id_leadership	INTEGER NOT NULL,
                name	TEXT,
                description	TEXT,
                img_path TEXT
            );
            INSERT INTO leaderships VALUES ('1','Personal', 'cepteur sint occaecat cupidatat non proident', 's');
            INSERT INTO leaderships VALUES ('2','Transformacional', 'cepteur sint occaecat cupidatat non proident', 's');
            INSERT INTO leaderships VALUES ('3','Democrático', 'cepteur sint occaecat cupidatat non proident', 's');
            INSERT INTO leaderships VALUES ('4','Transaccional', 'cepteur sint occaecat cupidatat non proident', 's');
            INSERT INTO leaderships VALUES ('5','Laissez-Faire', 'cepteur sint occaecat cupidatat non proident', 's');

        CREATE TABLE  user_history (
            id_user	TEXT NOT NULL,
            day	INTEGER NOT NULL,
            date	DATE NOT NULL,
            response TEXT,
            id_leadership INTEGER
        );
        INSERT INTO user_history VALUES ('1','1', '2020-03-24', 'loreipsum', '1');
        INSERT INTO user_history VALUES ('1','2', '2020-03-24', 'loreipsum', '1');
        INSERT INTO user_history VALUES ('1','3', '2020-03-24', 'loreipsum', '1');
        INSERT INTO user_history VALUES ('1','4', '2020-03-24', 'loreipsum', '1');
        INSERT INTO user_history VALUES ('1','21', '2020-04-20', 'loreipsum', '1');


        CREATE TABLE `goals` (
            `id_goal`	INTEGER PRIMARY KEY AUTOINCREMENT,
            `id_leadership`	TEXT NOT NULL,
            `description`	TEXT NOT NULL,
            `name`	TEXT NOT NULL
        );
        INSERT INTO goals VALUES ('3','1', 'test', 'test name');
        INSERT INTO goals VALUES ('4','1', 'test', 'test name');


        CREATE TABLE `goals_history` (
            `id_goal`	INTEGER NOT NULL,
            `id_user`	TEXT NOT NULL,
            `completed_date`	DATETIME NOT NULL,
            PRIMARY KEY(`id_goal`,`id_user`)
        );
        INSERT INTO goals_history VALUES ('1','1', '2020-04-6');
        INSERT INTO goals_history VALUES ('2','1', '2020-04-6');


        CREATE TABLE `texts` (
            `id_text` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            `name`	TEXT NOT NULL,
            `text`	TEXT NOT NULL,
            `type`	TEXT
        );

        INSERT INTO texts VALUES ('1','AppDescription', 'Para usar app tienes que hacer:', 'description');
        INSERT INTO texts VALUES ('2','AppDescription', 'uno, dos', 'description');
        INSERT INTO texts VALUES ('3','RoutinePage', 'Recomendación', 'description');

        CREATE TABLE routines ( 
            day INTEGER NOT NULL, 
            id_quote TEXT, 
            id_activity TEXT, 
            id_test TEXT, 
            id_leadership TEXT,
            PRIMARY KEY(day) 
        );
            INSERT INTO routines VALUES ('1','1', '1', '1', '1');
            INSERT INTO routines VALUES ('22','1', '1', '1', '2');


            CREATE TABLE activities (
                id_activity	INTEGER PRIMARY KEY AUTOINCREMENT,
                activity_description	TEXT NOT NULL
            );
            INSERT INTO activities VALUES ('1','Corre');


            CREATE TABLE `quotes` (
                `id_quote`	INTEGER PRIMARY KEY AUTOINCREMENT,
                `quote`	TEXT NOT NULL,
                `author`	TEXT
            );
            INSERT INTO quotes VALUES ('1','Tirame del dedo', 'Slava Komarovski');


            CREATE TABLE tests (
                `id_test`	INTEGER PRIMARY KEY AUTOINCREMENT,
                `base`	TEXT NOT NULL,
                `positive`	TEXT,
                `negative`	TEXT
            );
            INSERT INTO tests VALUES ('1','Salta', 'Corre', 'Baila'); 

            CREATE TABLE users (
                id_user	TEXT NOT NULL,
                user_name	TEXT NOT NULL,
                email	TEXT NOT NULL,
                password	TEXT NOT NULL,
                nationality	TEXT,
                postal_code	TEXT,
                profession	TEXT,
                gender	TEXT NOT NULL, 
                birth_date	DATE NOT NULL,
                creation_date	DATE NOT NULL,
                mantra TEXT,
                PRIMARY KEY(id_user)
            );
            INSERT INTO users VALUES ('1','Sergio', 'sergio@jaja.com', 'admin', 'a', 'a', 'a', 'a', '1990-02-04', '2020-03-21', 'askdjh'); 
 
           
        "
        );
    }

    public function testRegister()
    {
        $this->json('POST', 'api/register/user', [
            'id_user' => '2',
            'user_name' => 'maría',
            'email' => 'maría@jaja.com',
            'password' => 'admin',
            'gender' => 'f',
            'birth_date' => '2004-04-6',
            'creation_date' => '2020-04-6',
        ])->assertStatus(200);
    }

    public function testGetTexts()
    {
        $this->json('GET', 'api/texts/description')

            ->assertStatus(200)

            ->assertJson([
                [
                    'id_text' => '1',
                    'name' => 'AppDescription',
                    'text' => 'Para usar app tienes que hacer:',
                    'type' => 'description',
                ],
                [
                    'id_text' => '2',
                    'name' => 'AppDescription',
                    'text' => 'uno, dos',
                    'type' => 'description',
                ],

            ]);
    }

    public function testGetTextsRoutine()
    {
        $this->json('GET', 'api/texts/routine')

            ->assertStatus(200)

            ->assertJson([
                [
                    'id_text' => '3',
                    'text' => 'Recomendación',
                ]
            ]);
    }

    public function testGetGoals()
    {
        $this->json('GET', 'api/goals/1')

            ->assertStatus(200)
            ->assertJson([
                [
                    'id_goal' => '3',
                    'id_leadership' => '1',
                    'description' => 'test',
                    'name' => 'test name',
                ],
                [
                    'id_goal' => '4',
                    'id_leadership' => '1',
                    'description' => 'test',
                    'name' => 'test name',
                ]
            ]);
    }

    public function testGetGoalsHistory()
    {
        $this->json('GET', 'api/goals_history/1')

            ->assertStatus(200)
            ->assertJson([
                [
                    'id_goal' => '1',
                    'id_user' => '1',
                    'completed_date' => '2020-04-6',
                ],
                [
                    'id_goal' => '2',
                    'id_user' => '1',
                    'completed_date' => '2020-04-6',
                ],
            ]);
    }

  
    public function testGetCurrentLeadership()
        {
        $this->json('GET', 'api/1/leaderships/state')
            ->assertStatus(200)
            ->assertJson([
              
                    [ 
                    'id_leadership' => '1',
                    'description' => 'cepteur sint occaecat cupidatat non proident',
                    'name' => 'Personal',
                    'img_path' => 's',
                    'active' => true
                    ],
                    [ 
                        'id_leadership' => '2',
                        'description' => 'cepteur sint occaecat cupidatat non proident',
                        'name' => 'Transformacional',
                        'img_path' => 's',
                        'active' => true
                    ],
                    [ 
                        'id_leadership' => '3',
                        'description' => 'cepteur sint occaecat cupidatat non proident',
                        'name' => 'Democrático',
                        'img_path' => 's',
                        'active' => false
                    ],
                    [ 
                        'id_leadership' => '4',
                        'description' => 'cepteur sint occaecat cupidatat non proident',
                        'name' => 'Transaccional',
                        'img_path' => 's',
                        'active' => false
                    ],
                    [ 
                        'id_leadership' => '5',
                        'description' => 'cepteur sint occaecat cupidatat non proident',
                        'name' => 'Laissez-Faire',
                        'img_path' => 's',
                        'active' => false
                    ],

            ]);
            $this->json('GET', 'api/2/leaderships/state')
            ->assertStatus(200)
            ->assertJson([
              
                    [ 
                    'id_leadership' => '1',
                    'description' => 'cepteur sint occaecat cupidatat non proident',
                    'name' => 'Personal',
                    'img_path' => 's',
                    'active' => true
                    ],
                    [ 
                        'id_leadership' => '2',
                        'description' => 'cepteur sint occaecat cupidatat non proident',
                        'name' => 'Transformacional',
                        'img_path' => 's',
                        'active' => false
                    ],
                    [ 
                        'id_leadership' => '3',
                        'description' => 'cepteur sint occaecat cupidatat non proident',
                        'name' => 'Democrático',
                        'img_path' => 's',
                        'active' => false
                    ],
                    [ 
                        'id_leadership' => '4',
                        'description' => 'cepteur sint occaecat cupidatat non proident',
                        'name' => 'Transaccional',
                        'img_path' => 's',
                        'active' => false
                    ],
                    [ 
                        'id_leadership' => '5',
                        'description' => 'cepteur sint occaecat cupidatat non proident',
                        'name' => 'Laissez-Faire',
                        'img_path' => 's',
                        'active' => false
                    ],

            ]);
        }


    public function testGetRoutine()
    {
        $this->withExceptionHandling();
        $this->json('GET', 'api/path/1/1/1')

            ->assertStatus(200)
            ->assertJson(
                [
                    "activity" => [
                        [
                            "id_activity" => "1",
                            "activity_description" => "Corre"
                        ]
                    ],
                    "quote" => [
                        [
                            "id_quote" => "1",
                            "quote" => "Tirame del dedo",
                            "author" => "Slava Komarovski"
                        ]
                    ],
                    "test" => [
                        [
                            "response" => "loreipsum",
                        ]
                        ],
                        "currentDay" => "22",
                ]
            );
    }

    public function testPostHistory()
    {
        $this->json('POST', 'api/1/history', [
            'day' => '1',
            'date' => '2020-03-24',
            'response' => 'loreipsum',
            'id_leadership' => '1'
        ])
            ->assertStatus(200)
            ->assertJson([
                'id_user' => '1',
                'day' => '1',
                'date' => '2020-03-24',
                'response' => 'loreipsum',
                'id_leadership' => '1'
            ]);

        $this->json('GET', 'api/1/history')
            ->assertStatus(200)
            ->assertJson([
                'id_user' => '1',
                'day' => '1',
                'date' => '2020-03-24',
                'response' => 'loreipsum',
                'id_leadership' => '1'
            ]);
    }

    public function testPostMantra()
    {
        $this->json('POST', 'api/3/mantra', [
            'mantra' => 'askdjh',
        ])
            ->assertStatus(200)
            ->assertJson([
                'mantra' => 'askdjh',
            ]);

        $this->json('GET', 'api/1/mantra')
            ->assertStatus(200)
            ->assertJson([
                'mantra' => 'askdjh',
            ]);
    }

    public function testPutMantra()
    {
        $this->json('PUT', 'api/1/edited-mantra', [
            'mantra' => 'Bakio',
        ])
            ->assertStatus(200)
            ->assertJson([
                'mantra' => 'Bakio',
            ]);

        $this->json('GET', 'api/1/edited-mantra')
            ->assertStatus(200)
            ->assertJson([
                'mantra' => 'Bakio',
            ]);
    }
}
