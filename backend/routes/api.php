<?php

use Illuminate\Http\Request;
use Illuminate\Support\Collection;


//-------------------------------------------------
//  GET user info  | component ProfilePage | 
//-------------------------------------------------

Route::get('/users/{uid}', function ($uid) {
    // Realizamos la consulta de la base de datos
    $results = DB::select('select * from users where id_user=:uid ', [
        'uid' => $uid,
    ]);
    return response()->json($results[0], 200);
});

//-------------------------------------------------
//  Login user | component Login | 
//-------------------------------------------------
Route::post('/log/users', function () {
    $data = request()->all();
    $mailExist = DB::select('select * from users where upper(email)=upper(:email)', [
        'email' => $data['email'],
    ]);
    $mailExist = sizeof($mailExist);
    if ($mailExist == 0) {
        $results = [
            'message' => 'error_wrong_email',
        ];
        return response()->json($results, 200);
    } else {
        $passwordTrue = DB::select('select password from users where upper(email)=upper(:email)', [
            'email' => $data['email'],
        ]);
        $arr = [];
        foreach ($passwordTrue as $raw) {
            $arr[] = (array) $raw;
        }
        if ($data['password'] == $arr[0]['password']) {
            $results = DB::select('select id_user, user_name from users where upper(email)=upper(:email) ', [
                'email' => $data['email'],
            ]);
            return response()->json($results[0], 200);
        } else {
            $results =
                [
                    'message' => 'error_wrong_password',
                ];
        }

        return response()->json($results, 200);
    }
});

//-----------------------------------------------------------------------------------------------------------------------------------
//  POST new user to users  | component Register  |  
//-----------------------------------------------------------------------------------------------------------------------------------

Route::post('/register/user', function () {
    $data = request()->all();
    $mailExist = DB::select('select * from users where upper(email)=upper(:email)', [
        'email' => $data['email'],
    ]);
    $mailExist = sizeof($mailExist);
    if ($mailExist !== 0) {
        $results = [
            'message' => 'account_exists',
        ];
        return response()->json($results, 200);
    } else {
        DB::insert(
            "         INSERT INTO users (id_user, user_name, email, password, nationality, postal_code, profession, gender, birth_date, creation_date)
       VALUES (:id_user, :user_name, :email, :password, :nationality, :postal_code, :profession, :gender, :birth_date, :creation_date)
     ",
            $data
        );
        $results =  [
            'message' => 'ok',
        ];

        return response()->json($results, 200);
    }
});


//------------------------------------------------------------------------------------------------------
//  GET  nationality to list of nationalities | component Register | static type
//------------------------------------------------------------------------------------------------------

Route::get('/texts/nationalities', function (Request $request) {
    //bars_list (id, name, familyName, mobile, email)  a cambiar *
    $results = DB::select('select text from texts where name ="Register"');
    return response()->json($results[0], 200);
});

//------------------------------------------------------------------------------------------------------
//  GET  texts to list of texts | component AppDescription | static type
//------------------------------------------------------------------------------------------------------

Route::get('/texts/description', function (Request $request) {
    //bars_list (id, name, familyName, mobile, email)  a cambiar *
    $results = DB::select('select * from texts where name ="AppDescription"');
    return response()->json($results, 200);
});

//------------------------------------------------------------------------------------------------------
//  GET  texts to list of texts | component RoutinePage 
//------------------------------------------------------------------------------------------------------

Route::get('/texts/routine', function (Request $request) {
    $results = DB::select('select id_text, text from texts where name = "RoutinePage"');
    return response()->json($results, 200);
});

//--------------------------------------------------------------------------------------------------------------------
//  GET  goals to list of goals  |  component AppDescription |  recived leadership
//--------------------------------------------------------------------------------------------------------------------

Route::get('/goals/{id_leadership}', function ($id_leadership) {
    // Realizamos la consulta de la base de datos
    $results = DB::select('select * from goals where id_leadership=:id_leadership ', [
        'id_leadership' => $id_leadership,
    ]);
    return response()->json($results, 200);
});

//----------------------------------------------------------------------------------
//  GET goals_history | component GoalsPage  |  id_user 
//----------------------------------------------------------------------------------

Route::get('/goals_history/{id_user}', function ($id_user) {
    // Realizamos la consulta de la base de datos
    $results = DB::select('select * from goals_history where id_user=:id_user', [
        'id_user' => $id_user,
    ]);
    return response()->json($results, 200);
});

//-----------------------------------------------------------------------------------------------------------------------------------
//  POST new goal completed to goals_ history | component Goals  |  id_user and id_goal
//-----------------------------------------------------------------------------------------------------------------------------------

// Route::post('/goals_history', function () {
//     // Así se recogen los datos que llegan de la petición
//     $data = request()->all();
//     DB::insert(
//         "         INSERT INTO goals_history (id_goal, id_user, completed_date)
//       VALUES (:id_goal, :id_user, :completed_date)
//     ",
//         $data
//     );

//     $results = DB::select('select * from goals_history where id_user = :id_user and id_goal = :id_goal', [
//         'id_user' => $data['id_user'],
//         'id_goal' => $data['id_goal']
//     ]);

//     return response()->json($results[0], 200);
// });

// //--------------------------------------------------------------------------------------------------------------------------
// //  DELETE register  from goals_history | component Goals   |  id_user and id_goal
// //--------------------------------------------------------------------------------------------------------------------------

// Route::delete('/{id_user}/goals_history/{id_goal}', function ($id_user, $id_goal) {
//     // Devolvemos un error con status code 404 si no hay registros en la tabla
//     if (goalHistoryNoExists($id_user, $id_goal)) {
//         abort(404);
//     }

//     DB::delete('delete from goals_history where id_user=:id_user  and  id_goal=:id_goal', [
//         'id_user' => $id_user,
//         'id_goal' => $id_goal
//     ]);

//     return response()->json('', 200);
// });

// if (!function_exists('goalHistoryNoExists')) {
//     function goalHistoryNoExists($id_user, $id_goal)
//     {
//         $results = DB::select('select * from goals_history where id_user=:id_user  and  id_goal=:id_goal', [
//             'id_user' => $id_user,
//             'id_goal' => $id_goal
//         ]);
//         return count($results) == 0;
//     }
// }

//--------------------------------------------------------------------------
//  Get leadership's state per user
//--------------------------------------------------------------------------


Route::get('{id_user}/leaderships/state', function ($id_user) {
    $leaderships = DB::select('select id_leadership, name, description, img_path from leaderships');

    $currentLeadership = DB::select('select routines.id_leadership 
    from routines  
  where routines.day = (select MAX(day)+1 from user_history where id_user=:id_user)', [
        'id_user' => $id_user,
    ]);

    if ($currentLeadership == null) {
        $currentLeadership[0] =  (object)['id_leadership' => "1"];

        $leadershipsMode = getLeadershipsStatus($leaderships, $currentLeadership);
        return response()->json($leadershipsMode, 200);
    } else {
        $leadershipsMode = getLeadershipsStatus($leaderships, $currentLeadership);
        return response()->json($leadershipsMode, 200);
    }
});


if (!function_exists("getLeadershipsStatus")) {

    function getLeadershipsStatus($leaderships, $currentLeadership)
    {
        foreach ($leaderships as $leadership) {
            if ((int) $leadership->id_leadership <= (int) $currentLeadership[0]->id_leadership) {
                $leadership->active = true;
            } else {
                $leadership->active = false;
            }
        }
        return $leaderships;
    };
}


//--------------------------------------------------------------------------
//  Get leadership's state per user and id leadership
//--------------------------------------------------------------------------

Route::get('{id_user}/leaderships/{id_leadership}/state', function ($id_user, $id_leadership) {
    $currentDay = getCurrentDay($id_user, $id_leadership);
    $currentDate = DB::select('select date from user_history where id_user=:id_user  order by day desc limit 1', [
        'id_user' => $id_user,
    ]);

    //esta función de aquí hace que un usuario nuevo sin historial aún tenga una current date aleatoria, "2009-01-01"
    if ($currentDate == null) {
        $currentDate[0] =  (object)['date' => "2009-01-01"];
    }
    //Sustituye esa fecha aleatoria por lo que necesites, una función que capte la fecha de ese día, por ejemplo

    $leadershipName = DB::select('select leaderships.name from leaderships where id_leadership=:id_leadership', [
        'id_leadership' => $id_leadership
    ]);
    $newResult = [
        'currentDay' => $currentDay,
        'currentDate' => $currentDate[0]->date,
        'currentLeadershipName' => $leadershipName[0]->name,
    ];
    return response()->json($newResult, 200);
});

if (!function_exists("getCurrentDay")) {
    function getCurrentDay($id_user, $id_leadership)
    {
        $results = DB::select('select day from user_history where id_user=:id_user and id_leadership=:id_leadership order by day desc limit 1', [
            'id_user' => $id_user,
            'id_leadership' => $id_leadership
        ]);
        if ($results == null) {
            $dayOtherLeadership = DB::select('select day from user_history where id_user=:id_user order by day desc limit 1', [
                'id_user' => $id_user,
            ]);
            if ($dayOtherLeadership == null) {
                $currentDay = "1";
                return $currentDay;
            } else {
                $currentDay = $dayOtherLeadership[0]->day;
                $currentDay++;
                return $currentDay;
            }
        } else {
            $currentDay = $results[0]->day;
            $currentDay++;
            return $currentDay;
        };
    }
};

//--------------------------------------------------------------------------
//  Get List days  per id leadership
//--------------------------------------------------------------------------

Route::get('/routines/{id_leadership}', function ($id_leadership) {
    $results = DB::select('select day from routines where id_leadership=:id_leadership order by day', [
        'id_leadership' => $id_leadership
    ]);

    return $results;
});

//----------------------------------------------------------------------
//  GET routine | component RoutinePage  |  day 
//----------------------------------------------------------------------

Route::get('/path/{id_user}/{id_leadership}/{day}', function ($id_user, $id_leadership, $day) {
    $currentDay = getCurrentDay($id_user, $id_leadership);
    if ($currentDay == $day) {
        $test = DB::select('select *  
        from tests
        inner join routines
        on routines.id_test=tests.id_test
        where routines.day=:day', [
            'day' => $day,
        ]);
    } else {
        $test = DB::select('select response from user_history where day=:day and id_user=:id_user', [
            'day' => $day,
            'id_user' => $id_user,
        ]);
    };
    $activity = DB::select('select activities.id_activity, activities.activity_description, activities.activity_general  
    from activities
    inner join routines
    on routines.id_activity=activities.id_activity
    where routines.day=:day', [
        'day' => $day,
    ]);
    $quote = DB::select('select quotes.id_quote, quotes.quote, quotes.author  
    from quotes
    inner join routines
    on routines.id_quote=quotes.id_quote
    where routines.day=:day', [
        'day' => $day,
    ]);
    $newResult = [
        'activity' => $activity,
        'quote' => $quote,
        'test' => $test,
        'currentDay' => $currentDay,
    ];
    return response()->json($newResult, 200);
});

//-----------------------------------------------------------------------------------------------------------------------------------
//  POST history
//-----------------------------------------------------------------------------------------------------------------------------------

if (!function_exists("getHistory")) {
    function getHistory($id_user)
    {
        $results = DB::select('select * from user_history where id_user=:id_user', [
            'id_user' => $id_user,
        ]);
        return $results[0];
    };
};

Route::post('/{id_user}/history', function ($id_user) {
    $data = request()->all();
    $data["id_user"] = $id_user;

    DB::insert(

        "
            INSERT INTO user_history (id_user, day, date, response, id_leadership)
            VALUES (:id_user, :day, :date, :response, :id_leadership)
        ",
        $data
    );
    $result = getHistory($id_user);
    return response()->json($result, 200);
});

Route::get('/{id_user}/history', function ($id_user) {

    $result = getHistory($id_user);
    return response()->json($result, 200);
});
//----------------------------------------------------------------------
//  GET day history_user | component GoalsPage  |  leadership idUser
//----------------------------------------------------------------------

Route::get('/user_history/{id_user}/{id_leadership}', function ( $id_user,$id_leadership) {
    $results = DB::select('select * from user_history where id_leadership=:id_leadership and id_user=:id_user ', [
       'id_user' => $id_user ,
       'id_leadership' => $id_leadership
    ]);

    return $results;
});

//-----------------------------------------------------------------------------------------------------------------------------------
//  POST mantra to users | Este aún no se usa
//-----------------------------------------------------------------------------------------------------------------------------------

Route::post('/{id_user}/mantra', function () {
    $data = request()->all();
    DB::insert(
        "
        INSERT INTO users (mantra)
        VALUES (:mantra)
    ",
        $data
    );
    $results = DB::select('select mantra from users where id_user = :id_user', [
        'id_user' => $data['id_user']
    ]);

    return response()->json($results[0], 200);
});

//-----------------------------------------------------------------------------------------------------------------------------------
//  PUT mantra to users | Edit existing mantra
//-----------------------------------------------------------------------------------------------------------------------------------

Route::put('/{id_user}/edited-mantra', function () {
    $data = request()->all();
    DB::update(
        "
        UPDATE users
        SET mantra = :mantra
        WHERE id_user = :id_user;
    ",
        $data
    );
    $results = DB::select('select mantra from users where id_user = :id_user', [
        'id_user' => $data['id_user']
    ]);

    return response()->json($results[0], 200);
});
