import { shallowMount } from "@vue/test-utils";
import Positive from "@/components/RoutinePage/Positive.vue";

test("Crea componnete Positive y pinta las opciones como enlaces", () => {
  const wrapper = shallowMount(Positive, {
    propsData: {
      question: {
        question: "Color",
        answer: "Azul",
      },
    },
  });
  let link = wrapper.find("p");
  expect(link.is("p")).toBe(true);
});

test("Pinta la pregunta del test", () => {
  const wrapper = shallowMount(Positive, {
    propsData: {
      question: {
        question: "Color",
        answer: "Azul",
      },
    },
  });
  const statement = wrapper.find("h4");
  const textQuestion = statement.text();
  expect(textQuestion).toBe("Color");
});

test("Se envía test con la pregunta completada", async () => {
  const wrapper = shallowMount(Positive, {
    data() {
      return {
        completedQuestion: {
          question: "Color",
          answer: "Azul",
        },
      };
    },
    propsData: {
      question: {
        question: "Color",
        options: ["Azul", "Verde", "Amarillo"],
      },
    },
  });
  expect(wrapper.emitted()["emit-question-answers"]).toBe(undefined);
  const input = wrapper.find("textarea");
  input.trigger("keyup", {
    key: "a",
  });
  await wrapper.vm.$nextTick();
  expect(wrapper.emitted()["emit-question-answers"].length).toBe(1);
  expect(wrapper.emitted()["emit-question-answers"][0]).toEqual([
    {
      question: "Color",
      answer: "Azul",
    },
  ]);
});
