import { shallowMount } from "@vue/test-utils";
import NavTypes from "@/components/DaysMapPage/NavTypes.vue";

test("Crea el componente NavType.vue y pinta el botón con nombre de Liderazgo", () => {
    const wrapper = shallowMount(NavTypes, {
      mocks: {
        $route: {
          params: { id_user: 1 , id_leadership: 1},
        },
      },
      propsData: {
        currentDayInfo: {currentDay:6, currentDate: 'yyyy-mm-dd',currentLeadershipName: "Personal"},
      },
    });
    let button = wrapper.find("button");
    const leadershipName = wrapper.find("big");
    const text = leadershipName.text();
    expect(text).toBe("Personal");
    expect(button.is("button")).toBe(true);
  });