import { shallowMount } from "@vue/test-utils";
import SingleLeadership from "@/components/LeadershipsPage/SingleLeadership.vue";

test("Crea el componente SingleLeadership.vue y pinta el botón", () => {
  const wrapper = shallowMount(SingleLeadership, {
    propsData: {
      singleLeadership: {
        id: null,
        name: "Personal",
        description: "Contiene descripción estándar",
      },
      id: "1",
    },
  });
  let button = wrapper.find("button");
  expect(button.is("button")).toBe(true);
});

test("Pinta la descripción", () => {
  const wrapper = shallowMount(SingleLeadership, {
    propsData: {
      singleLeadership: {
        id: null,
        name: "Personal",
        description: "Contiene descripción estándar",
      },
      id: "1",
    },
  });
  expect(wrapper.text()).toContain("Contiene descripción estándar");
});

test("Comprueba el computed que activa  el botón si active es true", async () => {
  const wrapper = shallowMount(SingleLeadership, {
    propsData: {
      singleLeadership: {
        id: null,
        name: "Personal",
        description: "Contiene descripción estándar",
        active: true,
      },
      id: "1",
    },
  });
  await wrapper.vm.$nextTick();
  const disable = wrapper.vm.isDisabled;
  expect(disable).toBe(false);
});

test("Comprueba el computed que desactiva el botón si active es false", async () => {
  const wrapper = shallowMount(SingleLeadership, {
    propsData: {
      singleLeadership: {
        id: null,
        name: "Transaccional",
        description: "Contiene descripción estándar",
        active: false,
      },

      id: "1",
    },
  });
  await wrapper.vm.$nextTick();
  const disable = wrapper.vm.isDisabled;
  expect(disable).toBe(true);
});
