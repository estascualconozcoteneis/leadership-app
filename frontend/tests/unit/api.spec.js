import { shallowMount } from "@vue/test-utils";
import AppDescription from "@/components/AppDescriptionPage/AppDescriptionPage.vue";
import DaysMapPage from "@/components/DaysMapPage/DaysMapPage.vue";
import SingleDay from "@/components/DaysMapPage/SingleDay.vue";
import LeadershipsPage from "@/components/LeadershipsPage/LeadershipsPage.vue";
import SingleLeadership from "@/components/LeadershipsPage/SingleLeadership.vue";
import GoalsPage from "@/components/GoalsPage/GoalsPage.vue";
import SingleGoal from "@/components/GoalsPage/SingleGoal.vue";
import RoutinePage from "@/components/RoutinePage/RoutinePage.vue";
import Register from "@/components/WelcomeScreen/Register.vue";

import api from "@/api";

const finishAsyncTasks = () => new Promise(setImmediate);

test("Funciona el created(loadTexts) y carga datos de AppDescription", async () => {
  api.loadTexts = jest.fn();
  const mockText = [
    { id_text: "tutorial1", text: "Para usar app tienes que hacer:" },
    { id_text: "tutorial2", text: "uno ,dos" },
    { id_text: "tutorial3", text: "tres ,cuatro" },
    { id_text: "tutorial4", text: "Ya esta, ahora disfruta !" },
  ];
  api.loadTexts.mockReturnValue(mockText);
  const wrapper = shallowMount(AppDescription, {
    data() {
      return {
        page: 0,
        maxPage: null,
        descriptionText: [
          { id_text: "tutorial1", text: "Para usar app tienes que hacer:" },
        ],
      };
    },
  });
  await finishAsyncTasks();
  expect(api.loadTexts).toHaveBeenCalled();
  expect(wrapper.vm.descriptionText).toEqual(mockText);
});

test("Funciona el Created y carga informacion de currentDay", async () => {
  api.getCurrentDayInfo = jest.fn();
  api.getDaysList = jest.fn();
  const mock = {
    currentDay: 6,
    currentLeadershipName: "Personal",
    currentDate: "2020-03-24T10:37:36.436Z",
  };
  const mockList = [
    {
      day: 1,
    },
    {
      day: 2,
    },
  ];

  api.getCurrentDayInfo.mockReturnValue(mock);
  api.getDaysList.mockReturnValue(mockList);
  const wrapper = shallowMount(DaysMapPage, {
    mocks: {
      $route: {
        params: { id_user: 1 },
      },
    },
    data() {
      return {
        id_user: 1,
        url: "/api/",
        urlDaysRoutine: "/api/routines/",
        daysList: [],
        dateToday: null,
        currentDayInfo: [],
      };
    },
  });
  await finishAsyncTasks();
  expect(api.getCurrentDayInfo).toHaveBeenCalled();
  expect(wrapper.vm.currentDayInfo).toEqual(mock);
  expect(api.getDaysList).toHaveBeenCalled();
  expect(wrapper.vm.daysList).toEqual(mockList);
  let daysMap = wrapper.findAll(SingleDay).wrappers;
  expect(daysMap.length).toBe(2);
});

test("Funciona el Mounted y carga datos de activeLeaderships", async () => {
  api.loadLeaderships = jest.fn();
  const mockLeaderships = [
    { id: null, name: "Autocrático", description: null },
    { id: null, name: "Transaccional", description: null },
    { id: null, name: "Democrático", description: null },
    { id: null, name: "Transformacional", description: null },
    { id: null, name: "Laissez-Faire", description: null },
  ];

  api.loadLeaderships.mockReturnValue(mockLeaderships);

  const wrapper = shallowMount(LeadershipsPage, {
    mocks: {
      $route: {
        params: { id_user: 1 },
      },
    },
    data() {
      return {
        idCurrentUser: "1",
        urlLeaderships: "/api/leaderships/",
        urlCurrentLeaderships: "/api/leaderships/state/",
        activeLeaderships: null,
      };
    },
  });
  await finishAsyncTasks();
  expect(api.loadLeaderships).toHaveBeenCalled();
  expect(wrapper.vm.activeLeaderships).toEqual(mockLeaderships);
  let leaderships = wrapper.findAll(SingleLeadership).wrappers;
  expect(leaderships.length).toBe(5);
});

test("Funciona el Mounted y carga los textos de la rutina (descripción del test y respuesta negativa)", async () => {
  api.loadRoutineTexts = jest.fn();
  api.loadRoutine = jest.fn();
  const routineTexts = [{ text: "hello" }, { text: "no" }];
  const mockRoutine = {
    activity: "null",
    quote: "null",
    test: [{ response: "null" }],
    currentDay: 5,
  };

  api.loadRoutine.mockReturnValue(mockRoutine);
  api.loadRoutineTexts.mockReturnValue(routineTexts);
  const wrapper = shallowMount(RoutinePage, {
    mocks: {
      $route: {
        params: { id_user: 1, day: 1, id_leadership: 1 },
      },
    },
    data() {
      return {
        date: null,
        routine: null,
        base: null,
        positive: null,
        negative: null,
        testHistory: null,
        clicked: false,
        historyMode: false,
        routineTexts: null,
        testDescription: null,
      };
    },
  });

  await finishAsyncTasks();
  expect(api.loadRoutineTexts).toHaveBeenCalled();
  expect(wrapper.vm.routineTexts).toEqual(routineTexts);
});

test("Funciona el Mounted y carga datos de la rutina", async () => {
  api.loadRoutine = jest.fn();
  api.loadRoutineTexts = jest.fn();
  const routineTexts = [{ text: "hello" }, { text: "no" }];
  const mockRoutine = {
    activity: "null",
    quote: "null",
    test: [{ response: "null" }],
    currentDay: 5,
  };

  api.loadRoutineTexts.mockReturnValue(routineTexts);
  api.loadRoutine.mockReturnValue(mockRoutine);
  const wrapper = shallowMount(RoutinePage, {
    mocks: {
      $route: {
        params: { id_user: 1, day: 1, id_leadership: 1 },
      },
    },
    data() {
      return {
        url: "/api/",
        urlRoutinePage: "path/",
        urlHistory: "/history",
        routine: null,
        test: null,
        testHistory: null,
        clicked: false,
        historyMode: false,
      };
    },
  });

  await finishAsyncTasks();
  expect(api.loadRoutine).toHaveBeenCalled();
  expect(wrapper.vm.routine).toEqual(mockRoutine);
});

test("Funciona el created(getGoalsList) y carga datos del goals", async () => {
  api.getGoalsHistory = jest.fn();
  api.getGoalsList = jest.fn();
  const mockGoalsList = [
    {
      id_goal: "3",
      id_leadership: "1",
      description: "fulfill this goal",
      name: "Goal number one",
    },
    {
      id_goal: "4",
      id_leadership: "1",
      description: "fulfill this goal",
      name: "Goal number one",
    },
  ];

  api.getGoalsList.mockReturnValue(mockGoalsList);
  const wrapper = shallowMount(GoalsPage, {
    mocks: {
      $route: {
        params: { id_user: 1 },
      },
    },
    data() {
      return {
        goals: [],
      };
    },
  });
  await finishAsyncTasks();
  expect(api.getGoalsHistory).toHaveBeenCalled();
  expect(wrapper.vm.goals).toEqual(mockGoalsList);
});

test("Comprueba que llama a la función que hace post del usuario registrado", async () => {
  api.postUser = jest.fn();

  const wrapper = shallowMount(Register, {
    data() {
      return {
        user: {
          id_user: "null",
          user_name: "null",
          email: "null",
          password: "null",
          passwordConfirmation: "null",
          /*     postal_code: null,
        nationality: null,
        profession: null,
        gender: null,
        birth_date: null, */
          creation_date: "null",
        },
        agree: true,
        empty: false,
        errors: false,
      };
    },
  });
  let button = wrapper.find("button");
  await button.vm.$nextTick();
  await finishAsyncTasks();
  expect(api.postUser).toHaveBeenCalled();
});
