import { shallowMount } from "@vue/test-utils";
import Negative from "@/components/RoutinePage/Negative.vue";

test("Crea componnete Negative y pinta las opciones como enlaces", () => {
  const wrapper = shallowMount(Negative, {
    propsData: {
      advice: "Hola",
    },
  });
  expect(wrapper.text()).toMatch("Hola");
});
