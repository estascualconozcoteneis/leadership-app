import { shallowMount } from "@vue/test-utils";
import Register from "@/components/WelcomeScreen/Register.vue";

test("Crea el componente Register.vue", () => {
  const wrapper = shallowMount(Register, {
    data() {
      return {
        user: {
          id_user: null,
          user_name: null,
          email: null,
          password: null,
          postal_code: null,
          nationality: null,
          profession: null,
          gender: null,
          birth_date: null,
          creation_date: null,
        },
        userNameError: false,
        surnameError: false,
        telephoneError: false,
        telephoneInvalidError: false,
        emailError: false,
        emailInvalidError: false,
        error: false,
      };
    },
  });
  let input = wrapper.find("input");
  expect(input.is("input")).toBe(true);
});
/*
test("Si no escribimos un campo el error general y el especifico se activa", () => {
  const wrapper = shallowMount(Register, {
    data() {
      return {
        user: {
          id_user: null,
          user_name: "null",
          email: "null",
          password: "null",
          postal_code: "null",
          nationality: "null",
          profession: "null",
          gender: "null",
          birth_date: "null",
          creation_date: "null",
        },
        userNameError: false,
        surnameError: false,
        telephoneError: false,
        telephoneInvalidError: false,
        emailError: false,
        emailInvalidError: false,
        error: false,
      };
    },
  });

  expect(wrapper.vm.error).toBe(true);
}); */

/* test("Pinta el array", () => {
  const wrapper = shallowMount(Register, {
    mocks: {
      $route: {
        params: { id_user: 1 },
      },
    },
    data() {
      return {
        allLeaderships: [
          { id: null, name: "Autocrático", description: null },
          { id: null, name: "Transaccional", description: null },
          { id: null, name: "Democrático", description: null },
          { id: null, name: "Transformacional", description: null },
          { id: null, name: "Laissez-Faire", description: null },
        ],
        activeLeaderships: [
          { id: null, name: "Autocrático", description: null, active: true },
          { id: null, name: "Transaccional", description: null, active: true },
          { id: null, name: "Democrático", description: null, active: true },
          {
            id: null,
            name: "Transformacional",
            description: null,
            active: true,
          },
          { id: null, name: "Laissez-Faire", description: null, active: true },
        ],
      };
    },
  });

  let leaderships = wrapper.findAll(SingleLeadership).wrappers;

  expect(leaderships.length).toBe(5);
});

test("Cambia los estados de los leadership según currentLeadership", async () => {
  const MockApiCall = jest.fn();
  const MockApiCall2 = jest.fn();
  const mockLeaderships = [
    { id: null, name: "Autocrático", description: null },
    { id: null, name: "Transaccional", description: null },
    { id: null, name: "Democrático", description: null },
    { id: null, name: "Transformacional", description: null },
    { id: null, name: "Laissez-Faire", description: null },
  ];

  const mockCurrentDayLeadership = {
    currentDay: "55",
    currentLeadership: "Democrático",
  };

  MockApiCall.mockReturnValue(mockLeaderships);

  MockApiCall2.mockReturnValue(mockCurrentDayLeadership);

  const wrapper = shallowMount(Register, {
      mocks: {
      $route: {
        params: { id_user: 1 },
      },
    },
    data() {
      return {
        idCurrentUser: "1",
        urlLeaderships: "/api/leaderships/",
        urlCurrentLeaderships: "/api/leaderships/state/",
        allLeaderships: [],
        currentDayLeadership: [],
        activeLeaderships: [],
      };
    },
    methods: {
      ...Register.methods,
      loadLeaderships: MockApiCall,
      loadCurrentDayLeadership: MockApiCall2,
    },
  });
  await wrapper.vm.$nextTick();
  expect(MockApiCall).toHaveBeenCalled();
  expect(MockApiCall2).toHaveBeenCalled();
  expect(wrapper.vm.activeLeaderships).toStrictEqual([
    { id: undefined, name: "Autocrático", description: null, active: true },
    { id: undefined, name: "Transaccional", description: null, active: true },
    { id: undefined, name: "Democrático", description: null, active: true },
    {
      id: undefined,
      name: "Transformacional",
      description: null,
      active: false,
    },
    { id: undefined, name: "Laissez-Faire", description: null, active: false },
  ]);
});

test("Funciona el Mounted y carga datos de allLeaderships", async () => {
  const MockApiCall = jest.fn();
  const mockLeaderships = [
    { id: null, name: "Autocrático", description: null },
    { id: null, name: "Transaccional", description: null },
  ];

  MockApiCall.mockReturnValue(mockLeaderships);

  const wrapper = shallowMount(Register, {
    mocks: {
      $route: {
        params: { id_user: 1 },
      },
    },
    data() {
      return {
        idCurrentUser: "1",
        urlLeaderships: "/api/leaderships/",
        urlCurrentLeaderships: "/api/leaderships/state/",
        allLeaderships: [],
        currentDayLeadership: [],
      };
    },
    methods: {
      ...Register.methods,
      loadLeaderships: MockApiCall,
    },
  });
  await wrapper.vm.$nextTick();
  expect(MockApiCall).toHaveBeenCalled();
  expect(wrapper.vm.allLeaderships).toEqual(mockLeaderships);
});

test("Funciona el Mounted y carga datos del currentDayLeadership", async () => {
  const MockApiCall = jest.fn();
  const mockCurrentDayLeadership = {
    currentDay: "27",
    currentLeadership: "Autocrático",
  };

  MockApiCall.mockReturnValue(mockCurrentDayLeadership);
  const wrapper = shallowMount(Register, {
    mocks: {
      $route: {
        params: { id_user: 1 },
      },
    },
    data() {
      return {
        idCurrentUser: "1",
        urlLeaderships: "/api/leaderships/",
        urlCurrentLeaderships: "/api/leaderships/state/",
        allLeaderships: [],
        currentDayLeadership: [],
      };
    },
    methods: {
      ...Register.methods,

      loadCurrentDayLeadership: MockApiCall,
    },
  });

  await wrapper.vm.$nextTick();
  expect(MockApiCall).toHaveBeenCalled();
  expect(wrapper.vm.currentDayLeadership).toEqual(mockCurrentDayLeadership);
});
 */
