import { shallowMount } from "@vue/test-utils";
import SingleDay from "@/components/DaysMapPage/SingleDay.vue";

test("Crea el componente SingleDay.vue y pinta el botón", () => {
  const wrapper = shallowMount(SingleDay, {
    mocks: {
      $route: {
        params: { id_user: 1 },
      },
    },
    propsData: {
      currentDayInfo: {
        currentDay: 6,
        currentLeadershipName: "Personal",
      },
      singleDay: { day: "1" },
      index: 0,
      id_leadership: 1,
    },
  });
  let button = wrapper.find("button");
  expect(button.is("button")).toBe(true);
});

test("Pinta los números", () => {
  const wrapper = shallowMount(SingleDay, {
    mocks: {
      $route: {
        params: { id_user: 1 },
      },
    },
    propsData: {
      currentDayInfo: {
        currentDay: 6,
        currentLeadershipName: "Personal",
      },
      singleDay: { day: "3" },
      index: 2,
      id_leadership: 1,
    },
  });
  expect(wrapper.text()).toContain("Día 3");
});

/* test("Comprueba que el computed activa el botón si el Day es menor que el currentDay", async () => {
  const wrapper = shallowMount(SingleDay, {
    mocks: {
      $route: {
        params: { id_user: 1 },
      },
    },
    propsData: {
      currentDayInfo: {
        currentDay: 6,
        currentLeadershipName:"Personal" ,
      },
      singleDay: {day:"1"},
      index: 0,
      id_leadership: 1,
    },
  });
  await wrapper.vm.$nextTick();
  const disable = wrapper.vm.isDisabled;
  expect(disable).toBe(false);
});

test("Comprueba que el computed desactiva el botón si el currentDay es menor que el singleDay", async () => {
  const wrapper = shallowMount(SingleDay, {
    mocks: {
      $route: {
        params: { id_user: 1 },
      },
    },
    propsData: {
      currentDayInfo: {
        currentDay: 6,
        currentLeadershipName:"Personal" ,
      },
      singleDay: {day:"8"},
      index: 7,
      id_leadership: 1,
    },
  });
  await wrapper.vm.$nextTick();
  const disable = wrapper.vm.isDisabled;
  expect(disable).toBe(true);
});

 

test("Comprueba el computed 'isToday' si el Day NO es igual que el currentDay", async () => {
  const wrapper = shallowMount(SingleDay, {
    mocks: {
      $route: {
        params: { id_user: 1 },
      },
    },
    propsData: {
      currentDayInfo: {
        currentDay: 6,
        currentLeadershipName: "Personal",
      },
      singleDay: { day: "1" },
      index: 0,
      id_leadership: 1,
    },
  });
  await wrapper.vm.$nextTick();
  const today = wrapper.vm.isToday;
  expect(today).toBe(false);
});

test("Comprueba  el computed 'isToday' si el Day es igual que el currentDay", async () => {
  const wrapper = shallowMount(SingleDay, {
    mocks: {
      $route: {
        params: { id_user: 1 },
      },
    },
    propsData: {
      currentDayInfo: {
        currentDay: 6,
        currentLeadershipName: "Personal",
      },
      singleDay: { day: "6" },
      index: 5,
      id_leadership: 1,
    },
  });
  await wrapper.vm.$nextTick();
  const today = wrapper.vm.isToday;
  expect(today).toBe(true);
});
*/
test("Comprueba el computed 'isCompleted' si el Day es mayor que el currentDay", async () => {
  const wrapper = shallowMount(SingleDay, {
    mocks: {
      $route: {
        params: { id_user: 1 },
      },
    },
    propsData: {
      currentDayInfo: {
        currentDay: 6,
        currentLeadershipName: "Personal",
      },
      singleDay: { day: "11" },
      index: 10,
      id_leadership: 1,
    },
  });
  await wrapper.vm.$nextTick();
  const completed = wrapper.vm.isCompleted;
  expect(completed).toBe(false);
});

test("Comprueba  el computed 'isCompleted' si el Day es igual que el currentDay", async () => {
  const wrapper = shallowMount(SingleDay, {
    mocks: {
      $route: {
        params: { id_user: 1 },
      },
    },
    propsData: {
      currentDayInfo: {
        currentDay: 6,
        currentLeadershipName: "Personal",
      },
      singleDay: { day: "6" },
      index: 5,
      id_leadership: 1,
    },
  });
  await wrapper.vm.$nextTick();
  const completed = wrapper.vm.isCompleted;
  expect(completed).toBe(false);
});

test("Comprueba  el computed 'isCompleted' si el Day es menor que el currentDay", async () => {
  const wrapper = shallowMount(SingleDay, {
    mocks: {
      $route: {
        params: { id_user: 1 },
      },
    },
    propsData: {
      currentDayInfo: {
        currentDay: 6,
        currentLeadershipName: "Personal",
      },
      singleDay: { day: "1" },
      index: 0,
      id_leadership: 1,
    },
  });
  await wrapper.vm.$nextTick();
  const completed = wrapper.vm.isCompleted;
  expect(completed).toBe(true);
});

/*

test("Comprueba que el computed desactiva el botón si la fecha del último día completado es igual que currentDate", async () => {
  const wrapper = shallowMount(SingleDay, {
    mocks: {
      $route: {
        params: { id_user: 1 },
      },
    },
    propsData: {
      currentDayInfo: {
        currentDay: 10,
        currentDate: "2020-06-03 Tgsb 723473hwfj",
        currentLeadershipName: "Personal",
      },
      dateToday: "2020-06-03",
      convertedCurrentDate: "2020-06-03",
      singleDay: { day: "10" },
      id_leadership: 1,
    },
  });
  await wrapper.vm.$nextTick();
  const disabledUntilTomorrow = wrapper.vm.isDisabledUntilTomorrow;
  expect(disabledUntilTomorrow).toBe(true);
});


test("Comprueba que el computed NO desactiva el botón si la fecha del último día completado NO es igual que currentDate", async () => {
  const wrapper = shallowMount(SingleDay, {
    mocks: {
      $route: {
        params: { id_user: 1 },
      },
    },
    propsData: {
      currentDayInfo: {
        currentDay: 10,
        currentDate: "2020-06-03 Tgsb 723473hwfj",
        currentLeadershipName: "Personal",
      },
      dateToday: "2020-06-04",
      convertedCurrentDate: "2020-06-03",
      singleDay: { day: "10" },
      id_leadership: 1,
    },
  });
  await wrapper.vm.$nextTick();
  const disabledUntilTomorrow = wrapper.vm.isDisabledUntilTomorrow;
  expect(disabledUntilTomorrow).toBe(false);
});

 test("Pinta la frase 'It will be open tomorrow ", async () => {
  const wrapper = shallowMount(SingleDay, {
    mocks: {
      $route: {
        params: { id_user: 1 },
      },
    },
    propsData: {
      currentDayInfo: {
        currentDay: 10,
        currentLeadershipName: "Personal",
      },
      dateToday: "2020-06-04",
      convertedCurrentDate: "2020-06-04",
      singleDay: { day: 10 },
      index: 9,
      id_leadership: "1",
    },
  });
  await wrapper.vm.$nextTick();
  const message = wrapper.find("p");
  const text = message.text();
  expect(text).toBe("Estará abierto mañana");
  const disabledUntilTomorrow = wrapper.vm.isDisabledUntilTomorrow;
  expect(disabledUntilTomorrow).toBe(true);
}); */
