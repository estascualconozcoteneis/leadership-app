import { shallowMount } from "@vue/test-utils";
import Test from "@/components/RoutinePage/Test.vue";
import Positive from "@/components/RoutinePage/Positive.vue";
import TestTextPositive from "@/components/RoutinePage/TestTextPositive.vue";

test("Crea el componente Test.vue y pinta la descripción", () => {
  const wrapper = shallowMount(Test, {
    data() {
      return {
        completedQuestion: {
          answer: "Sí",
        },
        completedTest: [],
        clicked: false,
      };
    },
    propsData: {
      base: [
        { question: "Color", options: ["Sí", "No"] },
        { question: "Color", options: ["Sí", "No"] },
      ],
      positive: [{ question: "Color", answer: "azul" }],
      negative: "bla",
      testDescription: "Hola",
      id_leadership: "1",
      id_user: "1",
    },
  });

  expect(wrapper.text()).toMatch("Hola");
});

test("Cada vez que haces click en una opción de la pregunta inicial la variable completedTest se inicializa", async () => {
  const wrapper = shallowMount(Test, {
    data() {
      return {
        completedQuestion: {
          answer: "Sí",
        },
        completedTest: [
          {
            question: "Color",
            answer: "Azul",
          },
          {
            question: "Número",
            answer: "1",
          },
          {
            question: "Musketeer favorito:",
            answer: "María",
          },
        ],
        clicked: false,
      };
    },
    propsData: {
      base: [{ question: "Color", options: ["Sí", "No"] }],
      positive: [{ question: "Color", answer: "azul" }],
      negative: [{ question: "Color", answer: "amarillo" }],
      testDescription: "Hola",
      id_leadership: "1",
      id_user: "1",
    },
  });
  let option = wrapper.find("a");
  option.trigger("click");
  await wrapper.vm.$nextTick();
  expect(wrapper.vm.completedTest).toEqual([]);
});

test("La respuesta enviada por positive o negative es incluida en el array completedTest", async () => {
  const wrapper = shallowMount(Test, {
    data() {
      return {
        completedQuestion: {
          answer: "Sí",
        },
        completedTest: [],
        clicked: false,
      };
    },
    propsData: {
      base: [{ question: "Color", options: ["Sí", "No"] }],
      positive: [{ question: "Color", answer: "azul" }],
      negative: [{ question: "Color", answer: "amarillo" }],
      testDescription: "Hola",
      id_leadership: "1",
      id_user: "1",
    },
  });
  const positive = wrapper.findAll(Positive).wrappers;
  positive[0].vm.$emit("emit-question-answers", {
    question: "Color",
    answer: "azul",
  });
  await wrapper.vm.$nextTick();
  expect(wrapper.vm.completedTest).toEqual([
    { question: "Color", answer: "azul" },
  ]);
});

test("Si se cambia la repuesta esta se sobreescribe en el array completedTest", async () => {
  const wrapper = shallowMount(Test, {
    data() {
      return {
        completedQuestion: {
          answer: "Sí",
        },
        completedTest: [{ answer: "azul", id: "1" }],
        clicked: false,
      };
    },
    propsData: {
      base: [{ question: "Color", options: ["Sí", "No"] }],
      positive: [{ question: "Color", answer: "azul" }],
      negative: [{ question: "Color", answer: "amarillo" }],
      testDescription: "Hola",
      id_leadership: "1",
      id_user: "1",
    },
  });
  const positive = wrapper.findAll(Positive).wrappers;
  positive[0].vm.$emit("emit-question-answers", {
    answer: "rojo",
    id: "1",
  });
  await wrapper.vm.$nextTick();
  expect(wrapper.vm.completedTest).toEqual([{ answer: "rojo", id: "1" }]);
});

test("Se hace un filter de completedTest para eliminar respuestas vacías", async () => {
  const wrapper = shallowMount(Test, {
    data() {
      return {
        completedQuestion: {
          answer: "Sí",
        },
        completedTest: [
          { question: "Color", answer: "azul" },
          { question: "Color", answer: "" },
        ],
        clicked: false,
      };
    },
    propsData: {
      base: [{ question: "Color", options: ["Sí", "No"] }],
      positive: [{ question: "Color", answer: "azul" }],
      negative: [{ question: "Color", answer: "amarillo" }],
      testDescription: "Hola",
      id_leadership: "1",
      id_user: "1",
    },
  });
  wrapper.vm.filterArray();
  await wrapper.vm.$nextTick();
  expect(wrapper.vm.completedTest).toEqual([
    { question: "Color", answer: "azul" },
  ]);
});

test("Emite evento con el historial recibido de TestTextPositive ya", async () => {
  const wrapper = shallowMount(Test, {
    mocks: {
      $route: {
        params: { id_user: 1, id_leadership: 1 },
      },
    },
    data() {
      return {
        completedQuestion: {
          answer: "Sí",
        },
        completedTest: [
          {
            question: "Color",
            options: ["Azul", "Verde", "Amarillo"],
            answer: "Azul",
          },
          {
            question: "Número",
            options: ["1", "2", "13"],
            answer: "1",
          },
          {
            question: "Musketeer favorito:",
            options: ["Yeray", "Yassine", "María"],
            answer: "María",
          },
        ],
        clicked: true,
        finished: true,
      };
    },
    propsData: {
      base: [{ question: "Color", options: ["Sí", "No"] }],
      positive: [
        { question: "Color", answer: "azul" },
        { question: "Color", answer: "azul" },
        { question: "Color", answer: "azul" },
      ],
      negative: [{ question: "Color", answer: "amarillo" }],
      testDescription: "Hola",
      id_leadership: "1",
      id_user: "1",
    },
  });

  const positiveText = wrapper.findAll(TestTextPositive).wrappers;
  expect(wrapper.emitted()["emit-test-answers"]).toBe(undefined);

  positiveText[0].vm.$emit("emit-final-text", "bla");
  await wrapper.vm.$nextTick();
  expect(wrapper.emitted()["emit-test-answers"].length).toBe(1);
  expect(wrapper.emitted()["emit-test-answers"][0]).toEqual(["bla"]);
});
