import { shallowMount } from "@vue/test-utils";
import TestTextPositive from "@/components/RoutinePage/TestTextPositive.vue";

test("Crea el componente TestTextPositive.vue", () => {
  const wrapper = shallowMount(TestTextPositive, {
    data() {
      return {
        date: "2020-10-12",
      };
    },
    propsData: {
      completedTest: [
        { answer: "Color", id: "1" },
        { answer: "Color", id: "1" },
        { answer: "Color", id: "1" },
      ],
      id_user: "1",
      id_leadership: "1",
    },
  });
  let text = wrapper.find("p");
  expect(text.is("p")).toBe(true);
});

test("Emite evento con el texto completado, que será el historial", async () => {
  const wrapper = shallowMount(TestTextPositive, {
    data() {
      return {
        date: null,
      };
    },
    propsData: {
      completedTest: [
        { answer: "Color", id: "1" },
        { answer: "Color", id: "1" },
        { answer: "Color", id: "1" },
      ],
      id_user: "1",
      id_leadership: "1",
    },
  });
  wrapper.vm.date = "2020-10-12";
  expect(wrapper.emitted()["emit-final-text"]).toBe(undefined);
  const button = wrapper.find("button");
  button.trigger("click");
  await wrapper.vm.$nextTick();
  expect(wrapper.emitted()["emit-final-text"].length).toBe(1);
  expect(wrapper.emitted()["emit-final-text"][0]).toEqual([
    "Hoy día 2020-10-12, he cumplido con la actividad diaria. Me he sentido Color y me ha ayudado a Color. Mis siguientes pasos son Color.",
  ]);
});
