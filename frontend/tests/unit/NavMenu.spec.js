import { shallowMount } from "@vue/test-utils";
import NavMenu from "@/components/WelcomeScreen/NavMenu.vue";

test("Crea el componente NavMenu.vue y pinta la lista", () => {
  const wrapper = shallowMount(NavMenu, {
    data() {
      return { clicked: true };
    },
  });
  let list = wrapper.find("ul");
  expect(list.is("ul")).toBe(true);
});

test("Si la variable clicked está en false, al clickar el div del menú pasará a true", async () => {
  const wrapper = shallowMount(NavMenu, {
    data() {
      return { clicked: false };
    },
  });
  let div = wrapper.find(".menu");
  div.trigger("click");
  await wrapper.vm.$nextTick();
  expect(wrapper.vm.clicked).toBe(true);
  div.trigger("click");
  await wrapper.vm.$nextTick();
  expect(wrapper.vm.clicked).toBe(false);
});
