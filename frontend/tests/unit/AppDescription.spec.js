import { shallowMount } from "@vue/test-utils";
import AppDescription from "@/components/AppDescriptionPage/AppDescriptionPage.vue";
import api from "@/api";

const finishAsyncTasks = () => new Promise(setImmediate);

test("Comprobamos que información de data imprimimos en la pantalla", () => {
  //crear componente con las propiedades
  //comprobar que el nombre y el apellido estan en su sitio
  const wrapper = shallowMount(AppDescription, {
    data() {
      return {
        page: 0,
        maxPage: null,
        descriptionText: [
          { id_text: "tutorial1", text: "Para usar app tienes que hacer:" },
          { id_text: "tutorial2", text: "uno ,dos" },
          { id_text: "tutorial3", text: "tres ,cuatro" },
          { id_text: "tutorial4", text: "Ya esta, ahora disfruta !" },
        ],
      };
    },
  });
  //buscamos la clase de la plantilla html (find)
  const description = wrapper.find("span");
  const textDescription = description.text();
  expect(textDescription).toBe("Para usar app tienes que hacer:");
  expect(wrapper.vm.page).toBe(0);
});
test("button next changes page to next", async () => {
  const wrapper = shallowMount(AppDescription, {
    data() {
      return {
        page: 0,
        maxPage: 5,
        descriptionText: [
          { id_text: "tutorial1", text: "Para usar app tienes que hacer:" },
          { id_text: "tutorial2", text: "uno ,dos" },
          { id_text: "tutorial3", text: "tres ,cuatro" },
          { id_text: "tutorial4", text: "Ya esta, ahora disfruta!" },
        ],
      };
    },
  });
  expect(wrapper.emitted().click).toBe(undefined);

  const buttonNext = wrapper.find(".button-next");
  buttonNext.trigger("click");
  await wrapper.vm.$nextTick();
  expect(wrapper.vm.page).toBe(1);
  const description = wrapper.find("span");
  const textDescription = description.text();
  expect(textDescription).toBe("uno ,dos");
});

test("button back changes page to previous", async () => {
  const wrapper = shallowMount(AppDescription, {
    data() {
      return {
        page: 0,
        maxPage: 5,
        descriptionText: [
          { id_text: "tutorial1", text: "Para usar app tienes que hacer:" },
          { id_text: "tutorial2", text: "uno ,dos" },
          { id_text: "tutorial3", text: "tres ,cuatro" },
          { id_text: "tutorial4", text: "Ya esta, ahora disfruta !" },
        ],
      };
    },
  });
  expect(wrapper.emitted().click).toBe(undefined);
  const buttonNext = wrapper.find(".button-next");
  buttonNext.trigger("click");
  await wrapper.vm.$nextTick();
  expect(wrapper.vm.page).toBe(1);
  const buttonBack = wrapper.find(".button-back");
  buttonBack.trigger("click");
  await wrapper.vm.$nextTick();
  expect(wrapper.vm.page).toBe(0);
});
