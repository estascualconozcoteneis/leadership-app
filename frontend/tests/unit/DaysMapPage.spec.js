import { shallowMount } from "@vue/test-utils";
import DaysMapPage from "@/components/DaysMapPage/DaysMapPage.vue";

test("Crea el componente DaysMapPage con un nav", () => {
  const wrapper = shallowMount(DaysMapPage, {
    mocks: {
      $route: {
        params: { id_user: 1, id_leadership: 1 },
      },
    },
    data() {
      return {
        id_user: 1,
        url: "/api/",
        urlDaysRoutine: "/api/routines/",
        daysList: [1, 2, 3, 4, 5, 6],
        currentDayInfo: null,
      };
    },
  });

  expect(wrapper.vm.currentDayInfo).toBe(null);
});
