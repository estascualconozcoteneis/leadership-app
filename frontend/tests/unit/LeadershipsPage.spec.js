import { shallowMount } from "@vue/test-utils";
import LeadershipsPage from "@/components/LeadershipsPage/LeadershipsPage.vue";
import SingleLeadership from "@/components/LeadershipsPage/SingleLeadership.vue";

test("Crea el componente LeadershipsPage.vue", () => {
  const wrapper = shallowMount(LeadershipsPage, {
    mocks: {
      $route: {
        params: { id_user: 1 },
      },
    },
    data() {
      return {
        activeLeaderships: [],
      };
    },
  });

  let leaderships = wrapper.findAll(SingleLeadership).wrappers;

  expect(leaderships.length).toBe(0);
});
