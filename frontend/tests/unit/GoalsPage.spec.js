import { shallowMount } from "@vue/test-utils";
import GoalsPage from "@/components/GoalsPage/GoalsPage.vue";
import SingleGoal from "@/components/GoalsPage/SingleGoal.vue";
jest.useFakeTimers();

test("pinta toda la lista vacío", () => {
  const wrapper = shallowMount(GoalsPage, {
    mocks: {
      $route: {
        params: { id_user: 1 },
      },
    },
    propsData: {
      singleGoal: [],
    },
  });
  const singleGoal = wrapper.findAll(SingleGoal).wrappers;

  expect(singleGoal.length).toBe(0);
});

test("single goal contiene un goal de la lista", () => {
  const goals = [
    {
      id_goal: "3",
      id_leadership: "1",
      description: "fulfill this goal",
      name: "Goal number one",
    },
    {
      id_goal: "4",
      id_leadership: "1",
      description: "fulfill this goal",
      name: "Goal number one",
    },
  ];
  const wrapper = shallowMount(SingleGoal, {
    mocks: {
      $route: {
        params: { id_user: 1 },
      },
    },
    propsData: {
      singleGoal: goals[0],
    },
  });
  const singleGoal = wrapper.findAll(SingleGoal).wrappers;
  expect(singleGoal.length).toBe(1);
});
