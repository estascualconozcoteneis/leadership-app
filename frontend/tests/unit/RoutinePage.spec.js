import { shallowMount } from "@vue/test-utils";
import RoutinePage from "@/components/RoutinePage/RoutinePage.vue";
import Test from "@/components/RoutinePage/Test.vue";
import api from "@/api";

const finishAsyncTasks = () => new Promise(setImmediate);

test("Crea el componente RoutinePage.vue", () => {
  const wrapper = shallowMount(RoutinePage, {
    mocks: {
      $route: {
        params: { id_user: 1, day: 1, id_leadership: 1 },
      },
    },
    data() {
      return {
        url: "/api/",
        urlRoutinePage: "path/",
        urlHistory: "/history",
        routine: null,
        clicked: false,
      };
    },
  });

  expect(wrapper.vm.routine).toBe(null);
});

test("Imprime el botón que saca el test", () => {
  const wrapper = shallowMount(RoutinePage, {
    mocks: {
      $route: {
        params: { id_user: 1, day: 1, id_leadership: 1 },
      },
    },
    data() {
      return {
        url: "/api/",
        urlRoutinePage: "path/",
        urlHistory: "/history",
        routine: {
          activity: [{ id: 1, activity_description: "corre" }],
          quote: [{ id: 1, quote: "bla" }],
          test: [{ id: 1, quote: "bla" }],
        },
        clicked: false,
      };
    },
  });
  const button = wrapper.find("button");
  expect(button.is("button")).toBe(true);
});

test("Imprime el botón para ir atrás", () => {
  const wrapper = shallowMount(RoutinePage, {
    mocks: {
      $route: {
        params: { id_user: 1, day: 1, id_leadership: 1 },
      },
    },
    data() {
      return {
        url: "/api/",
        urlRoutinePage: "path/",
        urlHistory: "/history",
        routine: {
          activity: [{ id: 1, activity_description: "corre" }],
          quote: [{ id: 1, quote: "bla" }],
          test: [{ id: 1, quote: "bla" }],
        },
        clicked: true,
      };
    },
  });
  const button = wrapper.find("button");
  expect(button.is("button")).toBe(true);
});

test("Pinta la actividad y la frase", () => {
  const wrapper = shallowMount(RoutinePage, {
    mocks: {
      $route: {
        params: { id_user: 1, day: 1, id_leadership: 1 },
      },
    },
    data() {
      return {
        url: "/api/",
        urlRoutinePage: "path/",
        urlHistory: "/history",
        routine: {
          activity: [{ id: 1, activity_description: "corre" }],
          quote: [{ id: 1, quote: "bla" }],
          test: "bla",
        },
        clicked: false,
        history: false,
      };
    },
  });
  const activity = wrapper.find("#activity-box");
  expect(activity.text()).toBe("Actividad corre");

  const quote = wrapper.find("#quote-box");
  expect(quote.text()).toBe("Motivación bla");
});

test("Botón ir al test cambia variable clicked de false a true", async () => {
  const wrapper = shallowMount(RoutinePage, {
    mocks: {
      $route: {
        params: { day: 1 },
      },
    },
    data() {
      return {
        url: "/api/",
        urlRoutinePage: "path/",
        urlHistory: "/history",
        urlHistory: "/history",
        routine: {
          activity: [{ id: 1, activity_description: "corre" }],
          quote: [{ id: 1, quote: "bla" }],
          test: "bla",
        },
        clicked: false,
        history: false,
      };
    },
  });
  const button = wrapper.find("button");
  button.trigger("click");
  await wrapper.vm.$nextTick();
  expect(wrapper.vm.clicked).toBe(true);
});

test("Según el día clickado y la rutina que tocaría ese día la variable history cambia de false a true", async () => {
  const wrapper = shallowMount(RoutinePage, {
    mocks: {
      $route: {
        params: { day: 1 },
      },
    },
    data() {
      return {
        url: "/api/",
        urlRoutinePage: "path/",
        urlHistory: "/history",
        urlHistory: "/history",
        routine: {
          activity: [{ id: 1, activity_description: "corre" }],
          quote: [{ id: 1, quote: "bla" }],
          test: [{ base: "null", part_one: "null" }],
          currentDay: 1,
        },
        clicked: false,
        historyMode: false,
      };
    },
  });
  wrapper.vm.getTestOrHistory();
  await wrapper.vm.$nextTick();
  expect(wrapper.vm.historyMode).toBe(false);
});

test("Según el día clickado y la rutina que tocaría ese día la variable history cambia de false a true", async () => {
  const wrapper = shallowMount(RoutinePage, {
    mocks: {
      $route: {
        params: { day: 1 },
      },
    },
    data() {
      return {
        url: "/api/",
        urlRoutinePage: "path/",
        urlHistory: "/history",
        urlHistory: "/history",
        routine: {
          activity: [{ id: 1, activity_description: "corre" }],
          quote: [{ id: 1, quote: "bla" }],
          test: [{ response: "null" }],
          currentDay: 5,
        },
        clicked: false,
        historyMode: false,
      };
    },
  });
  wrapper.vm.getTestOrHistory();
  await wrapper.vm.$nextTick();
  expect(wrapper.vm.historyMode).toBe(true);
});

test("Botón ir al test cambia variable clicked de true a false", async () => {
  const wrapper = shallowMount(RoutinePage, {
    mocks: {
      $route: {
        params: { day: 1 },
      },
    },
    data() {
      return {
        url: "/api/",
        urlRoutinePage: "path/",
        urlHistory: "/history",
        routine: {
          activity: [{ id: 1, activity_description: "corre" }],
          quote: [{ id: 1, quote: "bla" }],
          test: "bla",
        },
        clicked: true,
      };
    },
  });
  const button = wrapper.find("button");
  button.trigger("click");
  await wrapper.vm.$nextTick();
  expect(wrapper.vm.clicked).toBe(false);
});
