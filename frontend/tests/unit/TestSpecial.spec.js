import { shallowMount } from "@vue/test-utils";
import TestSpecial from "@/components/RoutinePage/TestSpecial.vue";

test("computed canSend esta en false si hay algun elemento vacillo en array de respuestas", async () => {
  const wrapper = shallowMount(TestSpecial, {
    data() {
      return {
        receivedQuestions: [
          {
            id: "0",
            type: "textarea",
            question: "¿Tu pelicula favorita?",
          },
          {
            id: "1",
            type: "option",
            question: "¿Estas aqui?",
            options: ["Sí", "No"],
          },
          {
            id: "2",
            type: "feedback",
            question: "¿Que tal estas?",
            header: "me siento ...",
          },
          {
            id: "3",
            type: "option",
            question: "¿Cómo quiero afrontar esta nueva ruta?",
            options: ["bien", "azi nazi", "so so"],
          },
        ],
        completedTest: {
          questions: [],
          headers: [],
          types: [],
          answers: ["Avatar", "No"],
        },
      };
    },
    propsData: {
      base: [{ question: "ba" }, { question: "bi" }, { question: "bu" }],
      testDescription: "blah",
    },
  });
  await wrapper.vm.$nextTick();
  expect(wrapper.vm.canSend).toEqual(false);
});
test("computed canSend esta en true si no hay ningun elemento vacillo en array de respuestas, y todos los campos estan completados", async () => {
  const wrapper = shallowMount(TestSpecial, {
    data() {
      return {
        receivedQuestions: [
          {
            id: "0",
            type: "textarea",
            question: "¿Tu pelicula favorita?",
          },
          {
            id: "1",
            type: "option",
            question: "¿Estas aqui?",
            options: ["Sí", "No"],
          },
        ],
        completedTest: {
          questions: [],
          headers: [],
          types: [],
          answers: ["Avatar", "No", "claro que sí, guapi"],
        },
      };
    },
    propsData: {
      base: [{ question: "ba" }, { question: "bi" }, { question: "bu" }],
      testDescription: "blah",
    },
  });
  await wrapper.vm.$nextTick();
  expect(wrapper.vm.canSend).toEqual(true);
});
