import { shallowMount } from "@vue/test-utils";
import SingleGoal from "@/components/GoalsPage/SingleGoal.vue";
jest.useFakeTimers();

test("Crea el componente SingleGoal.vue y pinta el botón", () => {
  const wrapper = shallowMount(SingleGoal, {
    propsData: {
      singleGoal: {currentDay:6,currentLeadershipName:"Personal"},
      id_user: 1,
      goalsHistory:[],
    },
  });
  let button = wrapper.find("button");
  expect(button.is("button")).toBe(true);
});

test("Pinta la goal name", () => {
  const wrapper = shallowMount(SingleGoal, {
    propsData: {
      singleGoal: {
        id_goal: "3",
        id_leadership: "1",
        description: "fulfill this goal",
        name: "Goal number one",
      },
      id_user: 1,
      goalsHistory:[],
    },
  });
  const goalName = wrapper.find("big");
  const textName = goalName.text();
  expect(textName).toBe("Goal number one");
});

test("Pinta la descripción", () => {
  const wrapper = shallowMount(SingleGoal, {
    propsData: {
      singleGoal: {
        id_goal: "3",
        id_leadership: "1",
        description: "fulfill this goal",
        name: "Goal number one",
      },
      id_user: 1,
      goalsHistory:[],
    },
  });
  const description = wrapper.find("p");
  const textDescription = description.text();
  expect(textDescription).toBe("fulfill this goal");
});

test("si buttonChecked es FALSE , envia el evento post", async () => {
  const wrapper = shallowMount(SingleGoal, {
    propsData: {
      singleGoal: {
        id_goal: "3",
        id_leadership: "1",
        description: "fulfill this goal",
        name: "Goal number one",
      },
      id_user: 1,
      goalsHistory:[],
    },
    data() {
      return {
        isCompleted:  false ,
        
        showAnimaton: false
      }
    },
     },
   );
  expect(wrapper.emitted()["post-goal-performed-to-history"]).toBe(undefined);
  expect(wrapper.emitted()["delete-goal-performed-from-history"]).toBe(undefined);
   const button = wrapper.find("button");
   button.trigger("click"); 
   await wrapper.vm.$nextTick();
   jest.advanceTimersByTime(1000);
   expect(wrapper.emitted()["post-goal-performed-to-history"].length).toBe(1);
   expect(wrapper.emitted()["delete-goal-performed-from-history"]).toBe(undefined);
});

test("si buttonChecked es TRUE , envia el evento delete", async () => {
  const wrapper = shallowMount(SingleGoal, {
    propsData: {
      singleGoal: 
      {
      id_goal:3,
      id_leadership:1,
      description:"fulfill this goal",
      name:"Goal number one",
    },
    id_user: 1,
    goalsHistory:[]
  }, 
  goalsHistory:[
    {id_goal:"1",id_user:"1",completed_date:"2020-03-24 08:29:03"},
    {id_goal:"2",id_user:"1",completed_date:"2020-03-24 08:29:03"},
    {id_goal:"5",id_user:"1",completed_date:"2020-03-24 08:29:03"},
    {id_goal:"4",id_user:"1",completed_date:"2020-04-29"},
    {id_goal:"3",id_user:"1",completed_date:"2020-04-29"}
  ],
    data() {
      return {
        isCompleted:  true ,
        showAnimaton: false
      }
   },
    },
  );
   expect(wrapper.emitted()["delete-goal-performed-from-history"]).toBe(undefined);
   expect(wrapper.emitted()["post-goal-performed-to-history"]).toBe(undefined);
   const button = wrapper.find("button");
   button.trigger("click");
   await wrapper.vm.$nextTick();
   jest.advanceTimersByTime(1000);
   expect(wrapper.emitted()["post-goal-performed-to-history"]).toBe(undefined);
   expect(wrapper.emitted()["delete-goal-performed-from-history"].length).toBe(1);
});

