import Vue from "vue";
import vueRouter from "vue-router";
import LeadershipsPage from "@/components/LeadershipsPage/LeadershipsPage.vue";
import WelcomeScreen from "@/components/WelcomeScreen/WelcomeScreen.vue";
import ProfilePage from "@/components/ProfilePage/ProfilePage.vue";
import AppDescriptionPage from "@/components/AppDescriptionPage/AppDescriptionPage.vue";
import RoutinePage from "@/components/RoutinePage/RoutinePage.vue";
import GoalsPage from "@/components/GoalsPage/GoalsPage.vue";
import DaysMapPage from "@/components/DaysMapPage/DaysMapPage.vue";
import LoginPage from "@/components/app/Login.vue";
import Register from "@/components/app/Register.vue";

Vue.use(vueRouter);

const router = new vueRouter({
  mode: "history",
  routes: [{
      path: "/register",
      component: Register
    },
    {
      path: "/login",
      component: LoginPage
    },
    {
      path: "/leaderships/:id_user",
      component: LeadershipsPage
    },
    {
      path: "/goals/:id_user/:id_leadership",
      component: GoalsPage
    },
    {
      path: "/profile/:id_user",
      component: ProfilePage
    },
    {
      path: "/",
      component: WelcomeScreen
    },
    {
      path: "/routine/:id_user/:id_leadership/:day",
      component: RoutinePage
    },
    {
      path: "/tutorial",
      component: AppDescriptionPage
    },
    {
      path: "/days_map/:id_leadership/:id_user",
      component: DaysMapPage
    },
  ],
});

export default router;