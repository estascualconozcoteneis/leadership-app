BEGIN TRANSACTION;
DROP TABLE IF EXISTS "goals";
CREATE TABLE IF NOT EXISTS "goals" (
	"id_goal"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"id_leadership"	INTEGER NOT NULL,
	"description"	TEXT NOT NULL,
	"name"	TEXT NOT NULL
);
DROP TABLE IF EXISTS "goals_history";
CREATE TABLE IF NOT EXISTS "goals_history" (
	"id_goal"	INTEGER NOT NULL,
	"id_user"	TEXT NOT NULL,
	"completed_date"	DATETIME NOT NULL,
	PRIMARY KEY("id_goal","id_user")
);
DROP TABLE IF EXISTS "leadership";
CREATE TABLE IF NOT EXISTS "leadership" (
	"id_leadership"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"name"	TEXT NOT NULL,
	"description"	TEXT
);
DROP TABLE IF EXISTS "users";
CREATE TABLE IF NOT EXISTS "users" (
	"id_user"	TEXT NOT NULL,
	"user_name"	TEXT NOT NULL,
	"email"	TEXT NOT NULL,
	"password"	TEXT NOT NULL,
	"nationality"	TEXT,
	"postal_code"	TEXT,
	"profession"	TEXT,
	"gender"	TEXT,
	"birth_date"	DATE,
	"creation_date"	DATE NOT NULL,
	PRIMARY KEY("id_user")
);
DROP TABLE IF EXISTS "quotes";
CREATE TABLE IF NOT EXISTS "quotes" (
	"id_quote"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"quote"	TEXT NOT NULL
);
DROP TABLE IF EXISTS "activities";
CREATE TABLE IF NOT EXISTS "activities" (
	"id_activity"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"activity_description"	TEXT NOT NULL
);
DROP TABLE IF EXISTS "tests";
CREATE TABLE IF NOT EXISTS "tests" (
	"id_test"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"json"	TEXT NOT NULL
);
DROP TABLE IF EXISTS "texts";
CREATE TABLE IF NOT EXISTS "texts" (
	"id_text"	INTEGER PRIMARY KEY AUTOINCREMENT,
  "name" TEXT NOT NULL,
	"text"	TEXT NOT NULL
);
DROP TABLE IF EXISTS "routines";
CREATE TABLE IF NOT EXISTS "routines" (
	"day"	INTEGER NOT NULL,
	"id_frase"	TEXT,
	"id_activity"	TEXT,
	"id_test"	TEXT,
	PRIMARY KEY("day")
);
DROP TABLE IF EXISTS "user_history";
CREATE TABLE IF NOT EXISTS "user_history" (
	"id_user"	TEXT NOT NULL,
	"day"	INTEGER NOT NULL,
	"date"	DATE NOT NULL,
	"response"	TEXT,
	PRIMARY KEY("id_user","day")
);
INSERT INTO "goals" (id_leadership,description,name) VALUES ('1','consectetur','xxx');
INSERT INTO "goals" (id_leadership,description,name) VALUES ('1','adipiscing','xx');
INSERT INTO "goals" (id_leadership,description,name) VALUES ('2','cillum','xxx');
INSERT INTO "goals" (id_leadership,description,name) VALUES ('2','deserunt','xxx');
INSERT INTO "goals" (id_leadership,description,name) VALUES ('3','laborum','xx');
INSERT INTO "goals" (id_leadership,description,name) VALUES ('3','consequat','xx');
INSERT INTO "goals" (id_leadership,description,name) VALUES ('4','incididunt','xx');
INSERT INTO "goals" (id_leadership,description,name) VALUES ('4','cupidatat','xx');
INSERT INTO "goals" (id_leadership,description,name) VALUES ('5','occaecat','xx');
INSERT INTO "goals" (id_leadership,description,name) VALUES ('5','aute','xxx');
INSERT INTO "goals_history" (id_goal,id_user,completed_date) VALUES (1,'1','2020-03-24 08:29:03');
INSERT INTO "goals_history" (id_goal,id_user,completed_date) VALUES (2,'1','2020-03-24 08:29:03');
INSERT INTO "goals_history" (id_goal,id_user,completed_date) VALUES (3,'1','2020-03-24 08:29:03');
INSERT INTO "goals_history" (id_goal,id_user,completed_date) VALUES (4,'1','2020-03-24 08:29:03');
INSERT INTO "goals_history" (id_goal,id_user,completed_date) VALUES (5,'1','2020-03-24 08:29:03');
INSERT INTO "goals_history" (id_goal,id_user,completed_date) VALUES (1,'2','2020-03-24 08:30:25');
INSERT INTO "goals_history" (id_goal,id_user,completed_date) VALUES (2,'2','2020-03-24 08:30:25');
INSERT INTO "goals_history" (id_goal,id_user,completed_date) VALUES (3,'2','2020-03-24 08:30:25');
INSERT INTO "goals_history" (id_goal,id_user,completed_date) VALUES (4,'2','2020-03-24 08:30:25');
INSERT INTO "goals_history" (id_goal,id_user,completed_date) VALUES (5,'2','2020-03-24 08:30:25');
INSERT INTO "leadership" (name,description) VALUES ('Autocrático','cepteur sint occaecat cupidatat non proident');
INSERT INTO "leadership" (name,description) VALUES ('Transformacional','cepteur sint occaecat cupidatat non proident');
INSERT INTO "leadership" (name,description) VALUES ('Democrático','cepteur sint occaecat cupidatat non proident');
INSERT INTO "leadership" (name,description) VALUES ('Transaccional','cepteur sint occaecat cupidatat non proident');
INSERT INTO "leadership" (name,description) VALUES ('Laissez-faire','cepteur sint occaecat cupidatat non proident');
INSERT INTO "users" VALUES ('1','Sergio','sergio@soy.es','1234','española','48004','informatico','m','1970/07/25','2020-03-19 10:10:37');
INSERT INTO "users" VALUES ('2','Maria','maria@soy.es','1234','española','48010','informatico','f','1990/11/23','2020-03-19 10:12:46');
INSERT INTO "users" VALUES ('3','Cesar','cesar@soy.es','1234','española','48009','dentista','m','1980/11/03','2020-03-19 10:13:59');
INSERT INTO "users" VALUES ('4','Haimar','haimar@soy.es','4567','hondureña','48011','piloto','m','1980/01/11','2020-03-19 10:15:18');
INSERT INTO "users" VALUES ('5','Vlad','vlad@soy.es','9347','rusa','48009','dieñador','m','2003/12/22','2020-03-19 10:16:59');
INSERT INTO "users" VALUES ('6','Jose','jose@soy.es','1729','argentina','48005','futbolista','m','1993/02/02','2020-03-19 10:18:14');
INSERT INTO "users" VALUES ('7','Ziortza','ziortza@soy.es','1294','española','48020','profesora','f','1991/03/06','2020-03-19 10:19:28');
INSERT INTO "users" VALUES ('8','Naiara','naiara@soy.es','6812','española','48010','traductora','f','1996/10/16','2020-03-19 10:20:24');
INSERT INTO "users" VALUES ('9','Yeray','yeray@soy.es','6111','española','48011','boxeador','m','1992/11/11','2020-03-19 10:21:18');
INSERT INTO "users" VALUES ('10','Unai','unai@soy.es','5338','española','48006','politico','m','1997/12/19','2020-03-19 10:23:26');
INSERT INTO "quotes" (quote) VALUES  ('cepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum');
INSERT INTO "quotes" (quote) VALUES ('cepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum');
INSERT INTO "quotes" (quote) VALUES ('cepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum');
INSERT INTO "quotes" (quote) VALUES ('cepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum');
INSERT INTO "quotes" (quote) VALUES ('cepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum');
INSERT INTO "quotes" (quote) VALUES ('cepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum');
INSERT INTO "quotes" (quote) VALUES ('cepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum');
INSERT INTO "quotes" (quote) VALUES ('cepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum');
INSERT INTO "quotes" (quote) VALUES ('cepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum');
INSERT INTO "quotes" (quote) VALUES ('cepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum');
INSERT INTO "activities" (activity_description) VALUES ('correr');
INSERT INTO "activities" (activity_description) VALUES ('saltar');
INSERT INTO "activities" (activity_description) VALUES ('nadar');
INSERT INTO "activities" (activity_description) VALUES ('dormir');
INSERT INTO "activities" (activity_description) VALUES ('golpear');
INSERT INTO "activities" (activity_description) VALUES ('llorar');
INSERT INTO "activities" (activity_description) VALUES ('reir');
INSERT INTO "activities" (activity_description) VALUES ('leer');
INSERT INTO "activities" (activity_description) VALUES ('quemar');
INSERT INTO "activities" (activity_description) VALUES ('amar');
INSERT INTO "tests" (json) VALUES ('{

  "pregunta01" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

},

{

  "pregunta02" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

}

,{

  "pregunta03" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

},

{

  "pregunta04" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

},

{

  "pregunta05" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

}');
INSERT INTO "tests" (json) VALUES ('{

  "pregunta01" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

},

{

  "pregunta02" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

}

,{

  "pregunta03" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

},

{

  "pregunta04" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

},

{

  "pregunta05" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

}');
INSERT INTO "tests" (json) VALUES ('{

  "pregunta01" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

},

{

  "pregunta02" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

}

,{

  "pregunta03" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

},

{

  "pregunta04" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

},

{

  "pregunta05" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

}');
INSERT INTO "tests" (json) VALUES ('{

  "pregunta01" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

},

{

  "pregunta02" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

}

,{

  "pregunta03" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

},

{

  "pregunta04" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

},

{

  "pregunta05" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

}');
INSERT INTO "tests" (json) VALUES ('{

  "pregunta01" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

},

{

  "pregunta02" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

}

,{

  "pregunta03" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

},

{

  "pregunta04" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

},

{

  "pregunta05" : "A cada paso, un vino",

  "opcion1" : "Rioja Alavesa",

  "opcion2" : "Rutas",

  "opcion3" : "Amurrio",

  "opcion4" : "Costa Vasca",

  "opcion5" : "Gourmet"

}');
INSERT INTO "texts" (name,text) VALUES ('texto01','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua');
INSERT INTO "texts" (name,text) VALUES ('texto02','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua');
INSERT INTO "texts" (name,text) VALUES ('texto03','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua');
INSERT INTO "texts" (name,text) VALUES ('texto04','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua');
INSERT INTO "texts" (name,text) VALUES ('texto05','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua');
INSERT INTO "texts" (name,text) VALUES ('texto06','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua');
INSERT INTO "texts" (name,text) VALUES ('texto07','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua');
INSERT INTO "texts" (name,text) VALUES ('texto08','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua');
INSERT INTO "texts" (name,text) VALUES ('texto09','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua');
INSERT INTO "texts" (name,text) VALUES ('texto10','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua');
INSERT INTO "routines" VALUES (1,1,1,1);
INSERT INTO "routines" VALUES (2,1,1,1);
INSERT INTO "routines" VALUES (3,1,1,1);
INSERT INTO "routines" VALUES (4,1,1,1);
INSERT INTO "routines" VALUES (5,1,1,1);
INSERT INTO "routines" VALUES (6,1,1,1);
INSERT INTO "routines" VALUES (7,1,1,1);
INSERT INTO "routines" VALUES (8,1,1,1);
INSERT INTO "routines" VALUES (9,1,1,1);
INSERT INTO "routines" VALUES (10,1,1,1);
INSERT INTO "routines" VALUES (11,2,2,1);
INSERT INTO "routines" VALUES (12,2,2,1);
INSERT INTO "routines" VALUES (13,2,2,1);
INSERT INTO "routines" VALUES (14,2,2,1);
INSERT INTO "routines" VALUES (15,2,2,1);
INSERT INTO "routines" VALUES (16,2,2,1);
INSERT INTO "routines" VALUES (17,2,2,1);
INSERT INTO "routines" VALUES (18,2,2,1);
INSERT INTO "routines" VALUES (19,2,2,1);
INSERT INTO "routines" VALUES (20,2,2,1);
INSERT INTO "routines" VALUES (21,3,3,1);
INSERT INTO "routines" VALUES (22,3,3,2);
INSERT INTO "routines" VALUES (23,3,3,2);
INSERT INTO "routines" VALUES (24,3,3,2);
INSERT INTO "routines" VALUES (25,3,3,2);
INSERT INTO "routines" VALUES (26,3,3,2);
INSERT INTO "routines" VALUES (27,3,3,2);
INSERT INTO "routines" VALUES (28,3,3,2);
INSERT INTO "routines" VALUES (29,3,3,2);
INSERT INTO "routines" VALUES (30,4,4,2);
INSERT INTO "routines" VALUES (31,4,4,2);
INSERT INTO "routines" VALUES (32,4,4,2);
INSERT INTO "routines" VALUES (33,4,4,2);
INSERT INTO "routines" VALUES (34,4,4,2);
INSERT INTO "routines" VALUES (35,4,4,2);
INSERT INTO "routines" VALUES (36,4,4,2);
INSERT INTO "routines" VALUES (37,4,4,2);
INSERT INTO "routines" VALUES (38,4,4,2);
INSERT INTO "routines" VALUES (39,4,4,2);
INSERT INTO "routines" VALUES (40,5,5,2);
INSERT INTO "routines" VALUES (41,5,5,2);
INSERT INTO "routines" VALUES (42,5,5,2);
INSERT INTO "routines" VALUES (43,5,5,3);
INSERT INTO "routines" VALUES (44,5,5,3);
INSERT INTO "routines" VALUES (45,5,5,3);
INSERT INTO "routines" VALUES (46,5,5,3);
INSERT INTO "routines" VALUES (47,5,5,3);
INSERT INTO "routines" VALUES (48,5,5,3);
INSERT INTO "routines" VALUES (49,5,5,3);
INSERT INTO "routines" VALUES (50,6,6,3);
INSERT INTO "routines" VALUES (51,6,6,3);
INSERT INTO "routines" VALUES (52,6,6,3);
INSERT INTO "routines" VALUES (53,6,6,3);
INSERT INTO "routines" VALUES (54,6,6,3);
INSERT INTO "routines" VALUES (55,6,6,3);
INSERT INTO "routines" VALUES (56,6,6,3);
INSERT INTO "routines" VALUES (57,6,6,3);
INSERT INTO "routines" VALUES (58,6,6,3);
INSERT INTO "routines" VALUES (59,6,6,3);
INSERT INTO "routines" VALUES (60,7,7,3);
INSERT INTO "routines" VALUES (61,7,7,3);
INSERT INTO "routines" VALUES (62,7,7,3);
INSERT INTO "routines" VALUES (63,7,7,3);
INSERT INTO "routines" VALUES (64,7,7,4);
INSERT INTO "routines" VALUES (65,7,7,4);
INSERT INTO "routines" VALUES (66,7,7,4);
INSERT INTO "routines" VALUES (67,7,7,4);
INSERT INTO "routines" VALUES (68,7,7,4);
INSERT INTO "routines" VALUES (69,7,7,4);
INSERT INTO "routines" VALUES (70,8,8,4);
INSERT INTO "routines" VALUES (71,8,8,4);
INSERT INTO "routines" VALUES (72,8,8,4);
INSERT INTO "routines" VALUES (73,8,8,4);
INSERT INTO "routines" VALUES (74,8,8,4);
INSERT INTO "routines" VALUES (75,8,8,4);
INSERT INTO "routines" VALUES (76,8,8,4);
INSERT INTO "routines" VALUES (77,8,8,4);
INSERT INTO "routines" VALUES (78,8,8,4);
INSERT INTO "routines" VALUES (79,8,8,4);
INSERT INTO "routines" VALUES (80,9,9,4);
INSERT INTO "routines" VALUES (81,9,9,4);
INSERT INTO "routines" VALUES (82,9,9,4);
INSERT INTO "routines" VALUES (83,9,9,4);
INSERT INTO "routines" VALUES (84,9,9,4);
INSERT INTO "routines" VALUES (85,9,9,4);
INSERT INTO "routines" VALUES (86,9,9,4);
INSERT INTO "routines" VALUES (87,9,9,4);
INSERT INTO "routines" VALUES (88,9,9,4);
INSERT INTO "routines" VALUES (89,9,9,4);
INSERT INTO "routines" VALUES (90,10,10,5);
INSERT INTO "routines" VALUES (91,10,10,5);
INSERT INTO "routines" VALUES (92,10,10,5);
INSERT INTO "routines" VALUES (93,10,10,5);
INSERT INTO "routines" VALUES (94,10,10,5);
INSERT INTO "routines" VALUES (95,10,10,5);
INSERT INTO "routines" VALUES (96,10,10,5);
INSERT INTO "routines" VALUES (97,10,10,5);
INSERT INTO "routines" VALUES (98,10,10,5);
INSERT INTO "routines" VALUES (99,10,10,5);
INSERT INTO "routines" VALUES (100,10,10,5);
INSERT INTO "routines" VALUES (101,10,10,5);
INSERT INTO "routines" VALUES (102,10,10,5);
INSERT INTO "routines" VALUES (103,10,10,5);
INSERT INTO "routines" VALUES (104,10,10,5);
INSERT INTO "routines" VALUES (105,10,10,5);
INSERT INTO "user_history" VALUES ('1',1,'2020-03-24','loreipusm');
INSERT INTO "user_history" VALUES ('1',2,'2020-03-24','loreipusm');
INSERT INTO "user_history" VALUES ('1',3,'2020-03-24','loreipusm');
INSERT INTO "user_history" VALUES ('1',4,'2020-03-24','loreipusm');
INSERT INTO "user_history" VALUES ('1',5,'2020-03-24','loreipusm');
INSERT INTO "user_history" VALUES ('2',1,'2020-03-24','loreipusm');
INSERT INTO "user_history" VALUES ('2',2,'2020-03-24','loreipusm');
INSERT INTO "user_history" VALUES ('2',3,'2020-03-24','loreipusm');
INSERT INTO "user_history" VALUES ('2',4,'2020-03-24','loreipusm');
INSERT INTO "user_history" VALUES ('2',5,'2020-03-24','loreipusm');
COMMIT;
