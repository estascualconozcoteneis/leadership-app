const root = "/api/";
const rootRegister = root + "register/user";
const rootAppDescription = root + "texts/description";
const rootRegisterDescription = root + "texts/nationalities";
const rootDaysRoutine = root + "routines/";
const rootGoalsHistory = root + "goals_history/";
const rootGoalsList = root + "goals/";
const rootLeaderships = "/leaderships/state/";
const rootLoadRoutine = root + "path/";
const rootLoadCompletedDays = root + "user_history/";
const rootRoutineTexts = root + "texts/routine";
//const fetch = require("node-fetch");

export default {
  async _fetch(method, endpoint, data, headers) {
    const result = await fetch(endpoint, {
      method: method,
      body: JSON.stringify(data),
      headers: headers,
    });
    return await result.json();
  },

  // ====== Profile ======

  async getUserInfo(uid) {
    const result = await fetch(`${root}users/${uid}`);
    return await result.json();
  },

  async putMantra(mantra, id_user) {
    const result = await fetch(`${root}${id_user}/edited-mantra`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id_user,
        mantra,
      }),
    });
    return await result.json();
  },

  // ====== Login =======
  async login(email, password) {
    const result = await fetch(`${root}log/users/`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    });
    return await result.json();
  },

  // ======  WelcomePage  =======

  async postUser(user) {
    const result = await fetch(`${rootRegister}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id_user: user.id_user,
        user_name: user.user_name,
        email: user.email,
        password: user.password,
        birth_date: user.birth_date,
        gender: user.gender,
        postal_code: user.postal_code,
        profession: user.profession,
        nationality: user.nationality,
        creation_date: user.creation_date,
      }),
    });
    return await result.json();
  },

  // ======  Register  =======

  async loadNationalities() {
    const result = await fetch(`${rootRegisterDescription}`);
    return await result.json();
  },

  // ======  AppDescriptionPage  =======

  async loadTexts() {
    const result = await fetch(`${rootAppDescription}`);
    return await result.json();
  },

  // ======  DaysMapPage  =======

  async getCurrentDayInfo(id_user, id_leadership) {
    const result = await fetch(
      `${root}${id_user}/leaderships/${id_leadership}/state`
    );
    return await result.json();
  },

  async getDaysList(id_leadership) {
    const result = await fetch(`${rootDaysRoutine}${id_leadership}`);
    return await result.json();
  },

  // ======  GoalsPage  =======
  async loadCompletedDays(id_user, id_leadership) {
    const result = await fetch(
      `${rootLoadCompletedDays}${id_user}/${id_leadership}`
    );
    return await result.json();
  },

  async getGoalsHistory(id_user) {
    const result = await fetch(`${rootGoalsHistory}${id_user}`);
    return await result.json();
  },

  async getGoalsList(id_leadership) {
    const result = await fetch(`${rootGoalsList}${id_leadership}`);
    return await result.json();
  },

  async postGoalPerformed(id_single_goal, id_user, completed_date) {
    await fetch(`${rootGoalsHistory}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id_goal: id_single_goal,
        id_user: id_user,
        completed_date: completed_date,
      }),
    }).then((response) => response.json());
  },

  async deleteGoalPerformed(id_single_goal, id_user) {
    await fetch(`${root}${id_user}/goals_history/${id_single_goal}`, {
      method: "delete",
    }).then((response) => response.json());
  },

  // ======  LeadershipsPage  =======

  async loadLeaderships(id_user) {
    const result = await fetch(`${root}${id_user}${rootLeaderships}`);
    return await result.json();
  },

  // ======  RoutinePage  =======

  async loadRoutineTexts() {
    const result = await fetch(`${rootRoutineTexts}`);
    return await result.json();
  },

  async loadRoutine(id_user, id_leadership, day) {
    const response = await fetch(
      `${rootLoadRoutine}${id_user}/${id_leadership}/${day}`
    );
    return await response.json();
  },

  async postTest(id_user, day, date, response, id_leadership) {
    const result = await fetch(`${root}${id_user}/history`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id_user,
        day,
        date,
        response,
        id_leadership,
      }),
    });
    return await result.json();
  },

  //Este aún no se llama en front, no tenía acceso a routine
  async postMantra(mantra, id_user) {
    const result = await fetch(`${root}${id_user}/mantra`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id_user,
        mantra,
      }),
    });
    return await result.json();
  },
};
