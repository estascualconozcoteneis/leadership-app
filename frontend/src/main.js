import Vue from "vue";
import App from "./App.vue";
import vueRouter from "vue-router";
import router from "./routes";
import Vuelidate from "vuelidate";
import Loader from "./components/app/Loader";

Vue.config.productionTip = false;
Vue.component("Loader", Loader);
Vue.use(vueRouter);
Vue.use(Vuelidate);

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");