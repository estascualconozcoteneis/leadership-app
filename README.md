# leadership-app

A project aimed for people to become well-rounded leaders by following a route that includes tests, daily exercises and recommendations. Users create habits while mastering different leaderships, they will also be able to check their progress and reflect on their achievements.

## Technologies

- Vue v4.4.4
- Jest
- Vue Router
- PHP 7.2.24
- Laravel Framework 7.3.0

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint

```
## Authors

María Fernández - Vladislav Komarenko

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


